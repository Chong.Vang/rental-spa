import React from 'react'

export const Navbar = () => {
  return (
    <nav className='fixed mx-auto bg-[#2073d1] top-0 left-0 right-0 z-10'>
        <ul className='flex mx-5 py-4 justify-end'>
            <li className='text-white text-right mr-3'>Home</li>
            <li className='text-white text-right mr-3'>About</li>
            <li className='text-white text-right mr-3'>Location</li>
        </ul>
    </nav>
  )
}

export default Navbar;