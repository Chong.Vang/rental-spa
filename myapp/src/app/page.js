import { Climate_Crisis } from "next/font/google"
import Navbar from "./components/Navbar"


export default function Home() {
  return (
    <>
    <main className="flex min-h-screen bg-[#ffffff]">
      <Navbar />
    </main>
    </>
  )
}